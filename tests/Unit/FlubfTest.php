<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Flub;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Filesystem\FileNotFoundException;


class FlubfTest extends TestCase
{

/**
     * File loading
     *
     * @return void
     */
    public function testFile()
    {
        try {
            $file = Storage::disk('local')->get('flub'.'.'.Flub::FORMAT, 'Contents');
        } catch (FileNotFoundException $e) {
            throw new FileNotFoundException();
            $this->expectException(FileNotFoundException::class);
        }
        $this->assertTrue(true);
    }

    /**
    * File content
    *
    * @return void
    */
    public function testFileContent()
    {
        $flub = new Flub();
        $content = $flub->import('flub', Flub::FORMAT);
        $content = json_decode(json_encode($content), true);
 
        $this->assertArrayHasKey('url', $content[0]);
        $this->assertArrayHasKey('name', $content[0]);
    }
}
