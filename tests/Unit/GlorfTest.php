<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Glorf;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

class GlorfTest extends TestCase
{
    /**
     * File loading
     *
     * @return void
     */
    public function testFile()
    { 
       try{
        $file = Storage::disk('local')->get('glorf'.'.'.Glorf::FORMAT, 'Contents');
    } catch (FileNotFoundException $e) {
        throw new FileNotFoundException();
        $this->expectException(FileNotFoundException::class);
    }
    $this->assertTrue(true); 

    }

     /**
     * File content
     *
     * @return void
     */
    public function testFileContent()
    {   
        $glorf = new Glorf();
        $content = $glorf->import('glorf', Glorf::FORMAT);
        $content = json_decode(json_encode($content), true);
    
        $this->assertArrayHasKey('videos', $content);
        $this->assertArrayHasKey('tags', $content['videos'][0]);
        $this->assertArrayHasKey('url', $content['videos'][0]);
        $this->assertArrayHasKey('title', $content['videos'][0]);

    }




  

    


}
