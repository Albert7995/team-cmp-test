<?php

namespace App\Traits;

use Yaml;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

trait ImportTrait
{
    /**
     * Import the content of the file
     *
     * @return array
     */
    public function import($name, $format)
    {
        try {
            $file = Storage::disk('local')->get($name.'.'.$format, 'Contents');
   
            switch ($format) {
            case 'json':
            $contents = json_decode($file);
            return $contents;
            break;

            case 'yaml':
            $contents = Yaml::parse($file);
            return $contents;
            break;
        }
        } catch (FileNotFoundException $e) {
            exit("File ".$name.'.'.$format. " not found");
        }
    }
}
