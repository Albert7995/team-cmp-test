<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Flub;

class ImportFlub extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:flub';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import flub data';

    /**
     * The flub object.
     *
     * @var Flub
     */
    protected $flub;


    /**
     * Create a new command instance.
     *
     * @return void
     */


    public function __construct(Flub $flub)
    {
        parent::__construct();
        $this->flub = $flub;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->flub->readFile();
    }
}
