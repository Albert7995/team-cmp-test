<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Glorf;

class ImportGlorf extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:glorf';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import glorf data';

    /**
     * The glorf object.
     *
     * @var Glorf
     */
    protected $glorf;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Glorf $glorf)
    {
        parent::__construct();
        $this->glorf = $glorf;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->glorf->readFile();
    }
}
