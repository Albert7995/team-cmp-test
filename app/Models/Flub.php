<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ImportTrait;

class Flub extends Model
{
    use ImportTrait;

    const FORMAT = 'yaml';

    public function readFile()
    {
        $content = $this->import('flub', self::FORMAT);

        foreach ($content as $video) {
            $string = "Importing: ";

            $keys = array_keys($video);
            foreach ($keys as $key) {
                $string = $string.ucfirst($key).": ".$video[$key];
                if ($key != end($keys)) {
                    $string = $string.'; ';
                }
            }
            echo($string. PHP_EOL);
        }
    }
}
