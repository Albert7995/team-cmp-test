<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ImportTrait;

class Glorf extends Model
{
    const FORMAT = 'json';

    use ImportTrait;

    public function readFile()
    {
        $content = $this->import('glorf', self::FORMAT);
        $content = json_decode(json_encode($content), true);

        foreach ($content['videos'] as $video) {
            $string = "Importing: ";
        
            $keys = array_keys($video);
            foreach ($keys as $key) {
                if (is_array($video[$key])) {
                    $string = $string.ucfirst($key).": ";
                    foreach ($video[$key] as $attribute) {
                        $string = $string . $attribute;

                        if ($attribute != end($video[$key])) {
                            $string = $string.', ';
                        }
                    }
                } else {
                    $string = $string.ucfirst($key).": ".$video[$key];
                }
                if ($key != end($keys)) {
                    $string = $string.'; ';
                }
            }
            echo($string. PHP_EOL);
        }
    }
}
