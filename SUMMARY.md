# Video Share
The commands are created using Laravel Artisan. 
This application is made with Laravel Framework. 

## Requirements
- PHP version: `7.1`
- Laravel version: `5.7`

## Extra used libraries
Symfony Yaml reader bundle.

## Setup
1. Clone the project
2. Run `composer install` in console
3. Generate an app key:
   Run `php artisan key:generate` in console
5. Make sure we can access the resources in the project:
Run `php artisan storage:link` in console
6. Add the files to the /storage/app directory

## Instructions 
`php artisan import:flub` : This command import the data of the flub file.

`php artisan import:glorf` : This command import the data of the flob file.

## Tests
The test check if the file exists and if the files have the necessary content.

There are two tests files: GlorfTests and FlubTests.

Run the tests executing `vendor/bin/phpunit`

## Code
Models: The are two models: Glorf and Flub, located in the App\Models folder.

ImportTrait: The ImportTrait class are responsible of isolating the file loading code. It import the file from the local Storage disk, but it can be changed simply changing the parameter of the disk method.

Artisan Commands: There are two commands: ImportFlub and ImportGlorf.






